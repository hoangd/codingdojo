from random import choice
import random
class person(object):
	"""docstring for person"""
	def __init__(self, name,money):
		self.name = name
		self.hand = {}
		self.win = 0
		self.lose = 0
		self.worth = money
		self.started = money
	def display(self):
		print "-----------------------------------------"
		print "----Player's name : {}".format(self.name)
		print "----Player   won  : {} games".format(self.win)
		print "----Player   lost : {} games".format(self.lose)
		print "----Player's worth: {} Dollars".format(self.worth)
		print "----Player   lost : {} Dollars".format(self.started - self.worth)
		print "-----------------------------------------"
		return self
	def displayHand(self):
		print "--------------{}'s hand----------------".format(self.name)
		for key in self.hand:
			print str(key) + " : " + " : ".join(self.hand[key])
		print "-----------------------------------------"
	def dealerDisplay(self):
		key,item = choice(list(self.hand.items()))
		randitem = random.randint(0,len(item) - 1)

		return key,self.hand[key][randitem]
	def size(self):
		total = 0
		for key in self.hand:
			total += len(self.hand[key])
		return total
	def draw(self,deck):
		key,Type = deck.discard()	
		try:
			self.hand[key] += [Type]
		except:
			self.hand[key] = [Type]
		return [key,Type]
	


