import random
from random import choice

class Deck(object):
	"""docstring for deck"""
	def __init__(self):
		self.deck =  {x: ['heart','club','diamond','spade'] for x in [2,3,4,5,6,7,8,9,10,'Jack','Queen','King','Ace']}
	def draw(self,times=1):
		hand = {}
		try:
			while(times > 0):
				times -= 1
				key,item = choice(list(self.deck.items()))
				if(len(self.deck[key]) == 0):
					self.deck.pop(key)
					times += 1
					continue
				randitem = random.randint(0,len(item) - 1)
				try:
					hand[key].append(self.deck[key].pop(randitem))
				except:
					hand[key] = [self.deck[key].pop(randitem)]
			return hand
		except:
			print "there is no more card to draw."
			return hand
	def shuffle(self):
		print "shuffled"
		return self
	def discard(self):
		hand = self.draw()
		hand = hand.items()
		key  = hand[0][0]
		item = hand[0][1][0]
		return key,item
		# key,item = choice(list(self.deck.items()))
		# randitem = random.randint(0,len(item) - 1)
		# item = self.deck[key].pop(randitem)
		# if(len(deck[key]) == 0):
		# 	self.deck.pop(key)
		# return  key, item
	def append(self,key,item):
		if key in self.deck:
			if item in self.deck[key]:
				print "sorry, card is already in the pile"
			else:
				print "card added"
				self.deck[key].append(item)
		else:
			print "card added"
			self.deck[key] = [item]
		return self
	def display(self):
		for key in self.deck:
			print key, self.deck[key]
		return self
	def size(self):
		total = 0
		for key in self.deck:
				total += len(self.deck[key])
		return total



