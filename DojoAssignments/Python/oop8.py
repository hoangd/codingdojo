class Underscore(object):
    def map(self,list,function):
        newlist = []
        for i in list:
            newlist.append(function(i))
    def reduce(self,list,function):
        total  = 0
        for i in range(0,len(list)-1,2):
            total += function(list[i],list[i+1])
        return total
    def find(self,list,function):
        for item in list:
            if(function(item)):
                return item
        else:
            print "no such item in list"
    def filter(self,list,function):
        newlist =[]
        for i in list:
            if(function(i)):
                newlist.append(i)
        return newlist
    def reject(self,list,function):
        newlist = []
        for i in list:
            if(!function(i)):
                newlist.append(i)
        return newlist
# you just created a library with 5 methods! let's create an instance of our
# class
_ = Underscore() # yes we are setting our instance to a variable that is an underscore
evens = _.filter([1, 2, 3, 4, 5, 6], lambda x: x % 2 == 0)
print evens
# should return [2, 4, 6] after you finish implementing the code above
total = _.find([1,2,3,5],lambda x: x%2 == 0)
print total