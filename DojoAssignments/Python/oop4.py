class animal(object):
	"""docstring for animal"""
	def __init__(self, name,health):
		self.name = name
		self.health = health
	def walk(self):
		self.health -= 1
		return self
	def run(self):
		self.health -=5
		return self
	def display(self):
		print self.health,self.name
		return self

class dog(animal):
	"""docstring for dog"""
	def __init__(self,name):
		super(dog, self).__init__(name, 150)
	
	def pet(self):
		self.health += 5
		return self

class dragon(animal):
	"""docstring for dragon"""
	def __init__(self, arg):
		super(dragon, self).__init__()
		self.health = 150
	def fly(self):
		self.health -= 10
		return self
		
dog = dog('betty')

dog.display()