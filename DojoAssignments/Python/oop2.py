class car(object):
	"""docstring for car"""
	def __init__(self, price,speed,fuel,mileage):
		self.price = price
		self.speed = speed
		self.fuel = fuel 
		self.mileage = mileage
		if price > 10000:
			self.tax = 0.15
		else:
			self.tax = 0.12

	def display_all(self):
		print "Price: {}$\nSpeed: {}mph\nFuel: {}\nMileage: {}mpg\nTax:{}".format(self.price,self.speed,self.fuel,self.mileage,self.tax)

car1 = car(10001,123,"not full",1000)
car1.display_all()	