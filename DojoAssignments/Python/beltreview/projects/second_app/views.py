# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect,reverse
from django.contrib import messages
from django.http import Http404
from projects.first_app.models import Users
from .models import Books,Authors

def root(request):
	try:	
		user = Users.objects.get(id = request.session['id'])
		name = user.name;
		return render(request,"second_app/index.html",{'name':name})
	except:
		return redirect(reverse("home:root"))
def add(request):
	pass