
from django.conf.urls import url,include
from django.contrib import admin
from . import views
#app_name = home

urlpatterns = [
	url(r'^$',views.root,name = 'root'),
	url(r'^registration$',views.register,name = 'registration'),
	url(r'^login$',views.login, name = 'login'),
	url(r'^logout$',views.logout,name = 'logout')
]
