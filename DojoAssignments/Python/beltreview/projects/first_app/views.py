# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect,reverse
from django.contrib import messages
import re

from models import Users
def root(request):
	return render(request,'first_app/index.html')
def register(request):
	result = Users.objects.validate(request.POST)
	if(result['status'] == False):
		for error in result['errors']:
			messages.error(request,error)
	else:
		user = Users.objects.make(request.POST)
		print user
		messages.success(request,"Sign up success! Please log in")
	return redirect(reverse("home:root"))
def login(request):
	result = Users.objects.findUser(request.POST)
	if(result['status'] == False):
		for error in result['errors']:
			messages.error(request,error)
			return redirect(reverse("home:root"))
	else:
		user = Users.objects.get(email = request.POST['email'])
		print user.id
		request.session['id'] = user.id
	return redirect(reverse("book:root"))
	#after logging in, user are send to the second app....
def logout(request):
	request.session.flush()
	return redirect(reverse("home:root"))


