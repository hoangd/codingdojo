# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import re

from django.db import models
class UserManager(models.Manager):
	def validate(self,postData):
		result = {'status':True,'errors':[]}
		user = []
		if not postData['name'] or len(postData['name']) < 3:
			result['status'] = False
			result['errors'].append('First name must be 3 chars long')
		if not postData['alias'] or len(postData['alias']) < 3:
			result['status'] = False
			result['errors'].append('alias must be 3 chars long')
		if not postData['email'] or len(postData['email']) < 4 or not re.match(r'[^@]+@[^@]+\.[^@]+', postData['email']):
			result['status'] = False
			result['errors'].append('email must be valid')
		if not postData['password'] or len(postData['password']) < 8 or postData['password']!= postData['c_password']:
			result['status'] = False
			result['errors'].append('password must be valid')
		
		if result['status'] == True:
			user = Users.objects.filter(email=postData['email'])
		if len(user) != 0:
			result['status'] = False
			result['errors'].append('User already exists. Please try another email.')
		return result
	def findUser(self,postData):
		result = {'status': False, 'errors': [],}
		user = []
		if not postData['email'] or len(postData['email']) < 4 or not re.match(r'[^@]+@[^@]+\.[^@]+', postData['email']):
			result['status'] = False
			result['errors'].append('email must be valid')
		else:
			user = Users.objects.get(email = postData['email'])
			if(user and user.password == postData['password']):
				result['status'] = True
			else:
				result['status'] = False
				result['errors'].append('email or password is incorrect')
		return result
	def make(self,postData):
		user = Users.objects.create(
			name=postData['name'],
			alias=postData['alias'],
			email=postData['email'],
			password=postData['password'],
			)
		return user
class Users(models.Model):
	name = models.CharField(max_length=50)
	alias = models.CharField(max_length=50)
	email = models.CharField(max_length=50)	
	password = models.CharField(max_length=30)
	created_at = models.DateTimeField(auto_now_add = True)
	updated_at = models.DateTimeField(auto_now_add = True)
	objects = UserManager()