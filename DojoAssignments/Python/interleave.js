Array.prototype.interleve = function(arr) {
  var ilp = this.length + arr.length - 1;
  var tlp = this.length - 1;
  var alp = arr.length - 1;
  while (ilp > 0 && alp >= 0 && tlp >= 0) {
    if (alp > tlp) {
      this[ilp] = arr[alp];
      ilp--;
      alp--;
    } 
    else if (tlp > alp) {
      this[ilp] = this[tlp];
      ilp--;
      tlp--;
    } 
    else {
      this[ilp] = arr[alp];
      ilp--;
      alp--;
      this[ilp] = this[tlp];
      ilp--;
      tlp--;
    }
  }
};

var array1 = [1, 2, 3, 4, 5, 6];
var array2 = ['a','b','c', 'd'];

array1.interleve(array2);
console.log(array1);
