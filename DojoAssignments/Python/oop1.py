class bike(object):
	"""docstring for ClassName"""
	def __init__(self, price,max_speed):
		self.price = price
		self.max_speed = max_speed
		self.miles = 0
	def displayinfo(self):
		print self.price, self.max_speed,self.miles
		return self
	def ride(self):
		print "Riding"
		self.miles += 10
		return self
	def reverse(self):
		print "Reversing"
		self.miles -=5
		return self
		
bike1 = bike(500, 25)
bike2 = bike(500,25)
bike2.displayinfo()
bike1.displayinfo().ride().reverse().displayinfo()