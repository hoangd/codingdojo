from flask import Flask,render_template,redirect,request,flash,session
app = Flask(__name__)
app.secret_key = "hello"
@app.route('/')
def root():
	return render_template('root.html')

@app.route('/user', methods=['POST'])
def form():
	if(len(request.form['name']) < 1 or len(request.form['comment']) < 1):
		flash('name or comment cannot be empty')
		return redirect('/')
	else if (len(request.form['comment']) > 120):
		flash('comment is too long')
		return redirect('/')
	else: 
		flash('sucess')
		name = request.form['name']
		location = request.form['location']
		language = request.form['language']
		comment = request.form['comment']
		return render_template('form.html',name = name,location = location,language = language,comment = comment)
 						
app.run(debug=True)
