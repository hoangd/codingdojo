from flask import Flask,render_template,redirect,session,request
from random import randrange

app = Flask(__name__)
app.secret_key = "Secret"

@app.route('/')
def root():
	session['value'] = randrange(0,100)
	try:
		return render_template('template.html',result=session['result'])
	except:
		return render_template('template.html',result='')
@app.route('/usr',methods=['POST'])
def usr():
	session['ans'] = request.form['value']
	return redirect('/game')
@app.route('/game')
def game():
	print session['ans'], session['value']
	if(int(session['ans']) == int(session['value'])):
		session['result'] = "you got it!!!!!, want to play again?"
		return redirect('/')
	else:
		if(int(session['ans']) < int(session['value'])):
			session['result'] = "Higher!"
		else:
			session['result'] = "Lower!"
	return render_template('template.html',result=session['result'])


app.run(debug=True)