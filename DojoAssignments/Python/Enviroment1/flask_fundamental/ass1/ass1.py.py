from flask import Flask, render_template,request,redirect
app = Flask(__name__)

@app.route('/')
def root():
	return render_template('root.html')

@app.route('/ninja')
def ninja():
	return render_template('ninja.html')


@app.route('/dojos/new')
def new():
	return render_template('new.html')

@app.route('/users', methods=['POST'])
def user():
	print "hello, im getting info"
	name = request.form['name']
	email = request.form['email']
	return redirect('/')

app.run(debug=True) # run our server