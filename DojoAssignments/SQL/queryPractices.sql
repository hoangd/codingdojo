use world; 
#select * from languages where language = "Slovene" ;

#select language,name,percentage from languages
#join countries on countries.id = languages.country_id
#where language = "Slovene"
#order by percentage desc;

#select countries.name, count(cities.id) as number_of_cities
#from cities
#join countries on countries.id = cities.country_id
#roup by country_id
#order by number_of_cities desc;

#select countries.name, cities.name, cities.population from cities
#join countries on countries.id = cities.country_id
#where countries.name = "Mexico" and cities.population >= 500000
#order by cities.population desc

#select language,percentage,name  
#from languages
#join countries on countries.id = languages.country_id
#where percentage >= 89

#select name, population 
#from countries 
#where surface_area < 501 and population > 100000

#select name from countries
#where capital >= 500 and life_expectancy > 75 and government_form = "Constitutional Monarchy"

#select cities.name, countries.name
#from cities 
#join countries on countries.id = cities.country_id
#where cities.population > 500000 and district = "Buenos Aires"

select count(id) as number_countries, region
from countries 
group by region 
order by number_countries desc;
