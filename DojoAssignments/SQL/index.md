DC2100A APPLICATION FIRMWARE HOMEPAGE       {#mainpage}
=============================================

Demonstration Circuit DC2100A is a Bi-Directional Cell Balancer using two LTC®3300-1 ICs to achieve active balancing of up to 12 Li-Ion cells. The board uses a single LTC6804-2 Multi-Cell Addressable Battery Stack Monitor 
IC to measure cell voltages and two LTC3300-1 ICs to provide active cell balancing. The DC2100A-C contains a PIC18F47J53 microcontroller to communicate with the LTC3300-1 and LTC6804-2 ICs, as well as an LTC6820 isoSPI Interface IC for communication with DC2100A-D boards. Up to 7 DC2100A-D boards can be connected to a DC2100A-C to build a stacked system of 8 total boards. 

A graphical user interface (GUI) uses a USB interface to communicate with the DC2100A-C. The GUI controls the LTC3300-1 ICs allowing manual control of the charging/discharging of cells and reporting the voltage of each cell.  Cell balancing is achieved through the LTC3300-1 ICs by transferring charge from one or more cells per LTC3300-1 to the stack or from the stack to one or more cells per LTC3300-1.  Design files for this circuit board are available at http://www.linear.com/demo/DC2100A.

![Figure 1: DC2100A-C PCB](.\Include\dc2100A.jpg)

SOFTWARE OVERVIEW
-----------------
The DC2100A source files consist of 2 collections of code for the PIC18 on the DC2100A-C, and 2 collections of code for a Windows computer (XP/7/8 supported).
The source code was developed to satisfy 2 requirements:

1. The firmware must communicate with the GUI software to allow evaluation of the LTC3300-1 and LTC6804-2 ICs.  Note that the GUI has a special mode when attached to a SuperCap Demo System available to LTC sales personnel, which demonstrates the value of active balancing in a simple low-capacity system.

2. The firmware code must provide a reference de-sign for a battery monitor and balancing system using the LTC3300-1 and the LTC6804-2.  The algorithms in the source code provide solutions for the complexities of active balancing such that the user merely needs to interface them to SOC algorithms for their battery chemistry.

The interaction between this code is shown in Figure 2.
![Figure 2: DC2100A Code Communication Diagram](.\Include\DC2100A_Comm_Diagram.PNG)

APP FW ARCHITECTURE
-------------------
Figure 3 shows the architecture of the files that comprise the DC2100A App FW.  While all of the files are required to make the DC2100A function with the GUI and SuperCap Demo Systems, the outlined files form the foundation of a reference project for a battery monitor and balancing system using the LTC6804-2 and LTC3300-1.  The files are collected into four groups:

• PIC18 Driver Files provide access to the PIC18 hardware necessary to support the other code modules. These files are not detailed in this doxygen documentation, as a customer using the DC2100A code in an alternative microprocessor would need to replace these files with drivers specific to their system.

• LTC Driver Files provide access to the services of LTC parts.  They are intended to be independent of the DC2100A and the PIC18 hardware.  They provide an API to the LTC6804-2, LTC3300-1, and LTC1380 ICs. These files are detailed in this doxygen documentation.
 
• Reference Application Files are examples of how the LTC6804-2/LTC3300-1 would be used to monitor and balance a customer’s battery system.  They are intended to be independent of the DC2100A and the PIC18 hardware.  They provide Voltage monitoring, Temperature monitoring, Balancer control, and EEPROM data storage through the LTC6804-2/LTC3300-1. These files are detailed in this doxygen documentation.

• DC2100A Application Files are specific to the DC2100A demo board system.  They satisfy the requirement to demonstrate the demo circuit functionality through the GUI SW and to operate the Super-Cap Demo Systems.  These files are not detailed in this doxygen documentation as a customer will want to replace these files with their own scheduling, State of Charge, and communication code to interface to the reference design files.

The interaction between this code is shown in Figure 3.
![Figure 3: App FW Architecture](.\Include\DC2100A_FW_Architecture.PNG)

LTC DRIVER FILES
----------------
These files provide access to the services of LTC parts.  They are intended to be independent of the DC2100A and the PIC18 hardware.  The API to these drivers are detailed in this doxygen documentation included with the source code.

All of the LTC Driver Files are architected to allow their reuse with minimal modification.  Figure 3 shows the relationship between the LTC Driver code and the other code in a system.
The LTCxxxx.c and LTCxxxx_Registers.h files contain all of the details from the LTC datasheet that are necessary to use the device, but do not need to be exposed to the Application code wishing to use the driver.  Register names, bit positions of data in the registers, and data integrity checks are examples of datasheet details that would be found in these files.  It is not intended for these files to require modification, in order for the LTC Driver code to be reused.

The LTCxxxx_Config.h file contains macros that configure the LTC Driver to the particular system in which it’s being used.  It serves as documentation of what resources are necessary to use an LTC Driver file. HW driver functions and system Timer variables are examples of configuration items that would be located in this file.  Each LTCxxxx_Config.h is configured for the DC2100A, but examples are given in the comments for how the configuration options would be changed for other systems.

The LTCxxxx.h file contains the API to the LTC Driver.  It provides the details necessary for Application code to use the driver at a functional level.  While some datasheet details may be in the file, they are the details necessary for Application code to use the driver.  For example, the different sampling modes for the LTC6804-2 would be in this file as different Application code modules may need to sample at different rates.  The register bits that need to be modified to achieve these sample rates, however, would not be in this API file.

![Figure 4: LTC Driver Code Module Architecture](.\Include\DC2100A_LTC_Driver_Code_Architecture.PNG)