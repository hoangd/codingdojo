function forEach(arr,cb){
	var cbarr = [];
	for(var i = 0; i < arr.length; i++){
		cbarr.push(cb(arr[i]));
	}
	return cbarr;
}

function filter2(arr,cb){
	var sol = [];
	forEach(arr,function(num) {
		if (cb(num)) {
			sol.push(num);
		}
	})
	return sol;
}
function reduce(arr){
	var sum = 0;
	forEach(arr,function(num) {
		sum+=num;
	});
	return sum;
}
console.log(filter2([1,2,3,4,5,6], function(num) {
	return num > 3;
}));

console.log(reduce([1,2,3,4,5]))
